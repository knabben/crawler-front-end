package controllers

import (
	"github.com/revel/revel"
	elastigo "github.com/mattbaird/elastigo/lib"
)

type App struct {
	*revel.Controller
}

func (c App) Index() revel.Result {
	return c.Render()
}

func (c App) Search(query string) revel.Result {
	var result []elastigo.Hit = Find(query)
	return c.RenderJson(result)
}

func (c App) Count(query string) revel.Result {
	result := Count(query)
	return c.RenderJson(result)
}
