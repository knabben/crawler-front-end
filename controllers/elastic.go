package controllers

import (
	"flag"
	"fmt"
	"os"
	"encoding/json"
	//"encoding/base64"
	elastigo "github.com/mattbaird/elastigo/lib"
)

var (
	host *string = flag.String("host", "localhost", " host")
	c = elastigo.NewConn()
)


func Count(query string) (map[string]interface{}) {
	min := elastigo.Aggregate("min_price").Min("price")
	max := elastigo.Aggregate("max_price").Max("price")
	avg := elastigo.Aggregate("avg_price").Avg("price")
	area := elastigo.Aggregate("avg_area").Avg("area")
	term := elastigo.Aggregate("city").Terms("city.folded")
	term.Aggregates(min, max, avg, area)

	qry := elastigo.Search("rent").Aggregates(term)
	out, err := c.Search("rent", "fpolis", nil, qry)
	if err != nil {
		fmt.Printf("%s", err)
	}
	// From the hell this shit encodes the result on base64
	var data map[string]interface{}

	err = json.Unmarshal(out.Aggregations, &data)
	return data
}

func Find(query string) ([]elastigo.Hit) {
	c.Domain = *host

	result := map[string]interface{}{
		"query": map[string]interface{}{
			"match_all": map[string]string{},
		},
		"size": 30,
		"filter": map[string]interface{}{
			"exists": map[string]string{
				"field": "location",
			},
		},
	}
	// On empty search get all 100 first
	if query != "" {
		result = map[string]interface{}{
			"query": map[string]interface{}{
				"multi_match": map[string]interface{}{
					"type":	"most_fields",
					"query": query,
					"fields": []string{"type.folded", "city", "neighbourhood.folded", "room"},
				},
			},
			"filter": map[string]interface{}{
				"exists": map[string]string{
					"field": "location",
				},
			},
			"size": 30,
		}
	}

	out, _err := c.Search("rent", "fpolis", nil, result)
	if _err != nil {
		fmt.Println(_err)
	}
	return out.Hits.Hits
}

func exitIfErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err.Error())
		os.Exit(1)
	}
}

type Property struct {
	Room int
	Neighborhood string
	City string
}
