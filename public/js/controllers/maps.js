define(['app'], function(app) {
  app.controller('MapsCtrl', function($scope, $rootScope, Restangular) {

    $rootScope.$on("searching", doSearch);

    function doSearch(event, query) {
      var prop = Restangular.all('/App/Search');
      prop.getList({"query": query}).then(function(properties) {
        var i = 0;
        $scope.map.dynamicMarkers = _.map(properties, function(prop) {
          var coord = decodeGeoHash(prop._source.location);
          i++;
          return {
            "id": i,
            "latitude": coord.latitude[0],
            "longitude": coord.longitude[0]
          }
        });
      });
    }
    doSearch();

    $scope.map = {
      title: "Fpolis map",
      center: {latitude: -27.5969, longitude: -48.5495},
      zoom: 11
    };
  })

});


