define(['common'], function (angularAMD) {
  'use strict';
  var app = angular.module('finder', ['ngRoute', 'restangular', 'uiGmapgoogle-maps']);

  app.config(['$interpolateProvider', '$routeProvider',
    function($interpolateProvider, $routeProvider, $httpProvider) {
      $interpolateProvider.startSymbol('{[');
      $interpolateProvider.endSymbol(']}');

      $routeProvider.when('/maps', angularAMD.route({
        templateUrl: '/public/partials/maps.html',
        controllerUrl: "MapsCtrl"
      }));

      $routeProvider.otherwise({redirectTo: '/maps'});
  }]);

  // Main controllers
  app.controller('MenuCtrl', function($scope, $rootScope) {
    $scope.doSearch = function() {
      $rootScope.$broadcast("searching", $scope.search);
    }
  })


  function refine_interval(interval, cd, mask) {
    if (cd&mask)
      interval[0] = (interval[0] + interval[1])/2;
    else
      interval[1] = (interval[0] + interval[1])/2;
  }

  function decodeGeoHash(geohash) {
    var BASE32 = "0123456789bcdefghjkmnpqrstuvwxyz";
    var BITS = [16, 8, 4, 2, 1];
    var is_even = 1;
    var lat = []; var lon = [];
    var lat_err = 0; var lon_err = 0;
    lat[0] = -90.0;  lat[1] = 90.0;
    lon[0] = -180.0; lon[1] = 180.0;
    lat_err = 90.0;  lon_err = 180.0;
    for (var i=0; i<geohash.length; i++) {
      var c = geohash[i];
      var cd = BASE32.indexOf(c);
      for (var j=0; j<5; j++) {
        var mask = BITS[j];
        if (is_even) {
          lon_err /= 2;
          refine_interval(lon, cd, mask);
        } else {
          lat_err /= 2;
          refine_interval(lat, cd, mask);
        }
        is_even = !is_even;
      }
    }
    lat[2] = (lat[0] + lat[1])/2;
    lon[2] = (lon[0] + lon[1])/2;

    return { latitude: lat, longitude: lon};
  }

  app.controller('PropertyCtrl', function($scope, $rootScope, $window, Restangular) {
    var prop = Restangular.one('/App/Count');
    prop.get().then(function(properties) {
      $scope.aggs = properties.city.buckets;
    });
  });

  return angularAMD.bootstrap(app);
});
