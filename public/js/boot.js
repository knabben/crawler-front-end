require.config({
  baseUrl: "/public/bower_components/",

  paths: {
    'angular': 'angular/angular',
    'angular-route': 'angular-route/angular-route',
    'angularAMD': 'angularAMD/angularAMD',
    'angular-map': "angular-google-maps/dist/angular-google-maps",
    'restangular': "restangular/dist/restangular",
    'underscore': 'underscore/underscore',
    'geocode': 'geohash/geohash',

    'ngload': 'angularAMD/ngload',
    'common': '../js/common',
    'app': '../js/app',

    'MapsCtrl': '../js/controllers/maps'
  },

  shim: {
    'angular-route': ['angular'],
    'angularAMD': ['angular'],
    'angular-map': ['angular', 'ngload'],
    'restangular': ['angular', 'underscore', 'ngload'],
    'MapsCtrl': ['geocode', 'angular-map'],
    'ngload': ['angularAMD']
  },

  deps: ['app']
});
